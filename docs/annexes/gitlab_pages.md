---
outline: deep
---

# Gitlab Pages

Voici quelques instructions avec images pour la création d'une page Gitlab. Les étapes se résument à ceci:

1. Créer un projet Gitlab Pages/Plain Html sur Gitlab
2. S'assurer que le projet est publique
3. Cloner le repo sur votre ordinateur
4. Choisir un thème
5. Mettre le thème dans le dossier public de votre repo
6. Git push
7. Succès! 
8. Allez dans Deploy -­> Pages et vous trouverez l'URL de votre site

## Instructions détaillées

### 1.1 Créer un nouveau projet

![Alt text](images/1%20-%20Nouveau%20Projet%20A.png)
![Alt text](images/1%20-%20Nouveau%20Projet%20B.png)


### 1.2 Choisir Pages/Plain HTML

![Alt text](images/1%20-%20Choix%20Projet.png)

### 2. Nommer le projet et le rendre publique

![Alt text](images/2%20-%20Création%20Projet.png)

### 3. Cloner le projet sur votre ordinateur

![Alt text](images/3%20-%20Cloner.png)

### 4. Chercher un thème pour votre site

Exemples :
- https://www.free-css.com/free-css-templates
- https://templatemo.com/page/1

### 5. Mettre le thème dans le dossier *public* de votre repo

![Alt text](images/5%20-%20css%20dans%20public.png)

### 6. Mettre à jour le repo git

J'utilise (Christopher C.) [Git Extensions](https://gitextensions.github.io/)
, mais prenez ce que vous voulez (Oli = VSCode ou cmd).

![Alt text](images/6%20-%20Push.png)

### 7-8. Aller chercher le URL sur Gitlab dans Deploy puis Pages

![Alt text](images/8%20-%20Deploy.png)

![Alt text](images/8%20-%20URL.png)

![Alt text](images/8%20-%20Tadah.png)
