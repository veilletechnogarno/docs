---
# https://vitepress.dev/reference/default-theme-home-page
layout: home

hero:
  name: "Veille Techno"
  text: "Notes pour Veille Techno"
  tagline: 
  actions:
    - theme: brand
      text: Plan de cours
      link: /plan-de-cours
    - theme: brand
      text: Cours 1
      link: cours/cours1
    - theme: brand
      text: Cours 2
      link: cours/cours2
    - theme: brand
      text: Cours 3
      link: cours/cours3
#    - theme: brand
#      text: Cours 4
#      link: /cours4
#    - theme: brand
#      text: Cours 5
#      link: /cours5
#    - theme: brand
#      text: Cours 9
#      link: /cours9
#    - theme: brand
#      text: Cours 12
#      link: /cours12
features:
  - title: Tp1
    link : travaux/tp1
    details: Travail pratique 1 (individuel)

  - title: Ancients projets étudiants
    link : https://veilletechnogarno.gitlab.io/wiki/
    details: Les liens vers les anciens projets!

---

