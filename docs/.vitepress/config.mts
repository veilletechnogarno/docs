import { defineConfig } from 'vitepress'

// https://vitepress.dev/reference/site-config
export default defineConfig({
  base:"/docs/",
  title: "Veille Techno",
  description: "Notes pour Veille Techno",
  themeConfig: {
    // https://vitepress.dev/reference/default-theme-config
    nav: [
      {
        text: 'Travaux pratiques',
        items: [
          { text: 'TP1', link: '/travaux/tp1' },
          { text: 'TP2', link: '/travaux/tp2' },
          { text: 'TP3', link: '/travaux/tp3' },
          { text: 'TP4', link: '/travaux/tp4' }
        ]
      },
      { text: 'Anciens projets', link: 'https://veilletechnogarno.gitlab.io/wiki/' }
    ],

    sidebar: [
      {
        text: 'Documents généraux',
        items: [
          { text: 'Plan de cours', link: '/plan-de-cours' },
        ]
      },
      {
        text: 'Modules',
        items: [         
          { text: 'Cours 1', link: '/cours/cours1' },
          { text: 'Cours 2', link: '/cours/cours2' },
          { text: 'Cours 3', link: '/cours/cours3' }
        ]
      },
      {
        text: 'Travaux pratiques',
        items: [
          { text: 'TP1', link: '/travaux/tp1' },
          { text: 'TP2', link: '/travaux/tp2' },
          { text: 'TP3', link: '/travaux/tp3' },
          { text: 'TP4', link: '/travaux/tp4' }
        ]
      }
    ],
    search: {
      provider: 'local'
    }
  }
})
