---
outline: deep
aside: false
---

# Travail pratique 1

- <a href="../Tp1H25.pdf" target="_blank">Télécharger l'énoncé</a>
- Exemple de contenu : https://demotp1veilletechno-jpboucher-3e6aa2a467ca0bc38efff63d7d025d3c9.gitlab.io/
- Date de remise : **mercredi le 19 février avant 15h**