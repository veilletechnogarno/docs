---
outline: deep
---

# Travail pratique 2

- <a href="../Tp2H25.pdf" target="_blank">Télécharger l'énoncé</a>
- [Grille des propositions de projet](https://docs.google.com/document/d/1WepCazzQrA-pHcleB_Y4vyjAlZc1ndzr/edit?usp=sharing&ouid=101914884112510485401&rtpof=true&sd=true)
- Date de remise : **mercredi le 19 mars avant 15h**