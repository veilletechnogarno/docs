---
outline: deep
---
# Cours 4
# Planification
<!-- no toc -->
- Rappel : TP1 à finir pour la semaine prochaine
- [Présentation des projets](#présentation-des-projets)
- [Atelier de pensée créative](#atelier-de-pensée-créative)

# Présentation du projet de session

https://gitlab.com/veilletechnogarno/contenu/-/blob/main/ProjetSession.pdf?ref_type=heads

# Atelier de pensée créative

1. Divergence - À partir des lieux que vous connaissez sur le campus Garneau, générer des idées qui peuvent répondre au projet sans contrainte techno.
2. Divergence - À partir des technologies que vous connaissez, générer des idées qui peuvent répondre au projet sans la contrainte du campus.
3. Convergence - En croisant les résultats de #1 et #2, convergez vers des débuts d'idées possibles.
4. Convergence - Conservez un maximum de 2 idées de #3. Pour chacune de ces idées :
   1.  Situez la précisément dans le campus Garneau.
   2.  Nommez au moins 3 objets matériels qui seraient utilisés.
   3.  Nommez la responsabilité de la partie logicielle.