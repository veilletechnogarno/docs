---
outline: deep
---
# Cours 3
## Planification
<!-- no toc -->
- [Dernières consignes du TP1](#dernieres-consignes-tp1)
- [Projet de session](#projet-de-session)
- [Énoncé du TP2](#enonce-du-tp2)

## Dernières consignes TP1

### Expérimentation
- Prendre des photos.
- Valides les cas d'exception. (À partir de quelle distance ça ne fonctionne plus, est-ce que ça fonctionne dans le noir, est-ce que ça fonctionne quand ça va vite, ...)

### Références
- Inclure les liens vers vos sources dans votre texte (lorsque l'info vient d'ailleurs).
- Le format n'est pas évalué. Tant que je puisse retrouver l'auteur. Par exemple:
    - "Selon [Tom's Hardware](https://www.tomshardware.com/pc-components/liquid-cooling/rtx-4090-liquid-cooled-with-12-000-btu-air-conditioner-rtx-5090-up-next-gpu-runs-at-20c) la RTX 5090 roule à 20 degrés C."
    - "La RTX 5090 roule à 20 degrés C ([Source](https://www.tomshardware.com/pc-components/liquid-cooling/rtx-4090-liquid-cooled-with-12-000-btu-air-conditioner-rtx-5090-up-next-gpu-runs-at-20c))"".
- Donc pas besoin d'une page à part pour les références.

## Projet de session
- Vos équipes **doivent** être formées d'ici le cours de la semaine prochaine (semaine 5).
- [Projet de session](https://docs.google.com/presentation/d/1CqBdn1Z-JTxE7k9sIPLwlVBTviE3HW864tJoHgjRPMk/edit?usp=sharing)

## Énoncé du TP2
- [Travail pratique 2](../travaux/tp2)

