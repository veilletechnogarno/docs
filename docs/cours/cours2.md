---
outline: deep
---
# Cours 2
# Planification
- [Sources d'information](#sources-dinformation)
- [Gitlab Pages](#gitlab-pages)
- [Précisions TP1](#précision-tp1)

## **Sources d'information**

### **Types de sources d'information**
1. **Source primaire** : L’information est directe et produite par la source elle-même.
   - **Exemples** : Rapport de laboratoire, témoin d’un événement, preuve manuscrite/vidéo/audio.
2. **Source secondaire** : L’information est retransmise ou interprétée par un intermédiaire.
   - **Exemples** : Articles vulgarisant un rapport, résumés de témoignages.

---

### **Avantages et inconvénients des sources**
| **Type de source**   | **Avantages**                          | **Inconvénients**                     |
|-----------------------|----------------------------------------|----------------------------------------|
| **Source primaire**   | Précise, détaillée, contextualisée     | Anecdotique, moins généralisable       |
| **Source secondaire** | Généralisable, vue d’ensemble          | Moins précise, parfois imprécise       |

- **Exemple** :  
   - Témoin unique d’un accident = source primaire, donne des détails précis.  
   - Policier ayant recueilli plusieurs témoignages = source secondaire, offre une analyse généralisée.

> **Note** : Il existe un **continuum** entre primaire et secondaire. Plus on généralise, plus on sacrifie les détails et le contexte.

  
---

### **Pertinence d’une source d’information**

Une source peut être **primaire** ou **secondaire**, mais cela ne garantit pas sa pertinence. Il est essentiel d’évaluer sa pertinence selon quatre critères :

1. **Provenance**  
   - Vérifiez si la source est signée et si l’auteur est identifiable et reconnu.  
   - Une source anonyme ou non signée est souvent moins crédible.  

2. **Fiabilité**  
   - Le contenu est-il corroboré par d’autres sources ?  
   - La source cite-t-elle des références fiables ?  
   - La présentation (structure, orthographe) est-elle soignée ?  

3. **Objectif**  
   - Quelle est l’intention de la source : informer, vendre, influencer ?  
   - Méfiez-vous des contenus trop partisans ou biaisés.  
   - Un contenu nuancé et équilibré est souvent plus crédible.  

4. **Temporalité**  
   - La source est-elle à jour ? Une information pertinente hier peut être dépassée aujourd’hui.  
   - La date de parution est-elle indiquée ?  

---

### **Outils de veille numériques**

#### **Sites d'actualités**
- [Hacker News](https://news.ycombinator.com/)  
- [Slashdot](https://slashdot.org/)  
- [Reddit Technology](https://www.reddit.com/r/technology/)  

#### **Bases de dépôts de brevets**
- [Google Scholar](https://scholar.google.ca/)  

#### **Sites spécialisés**
- [Tom's Hardware](https://www.tomshardware.com/)  

#### **Podcasts**
- [**Tech Café**](https://techcafe.fr/) : Actualité technologique et analyses approfondies.  
- [**Culture Numérique**](https://www.siecledigital.fr/podcasts/) : Impact des technologies sur la société et les 

#### **Chaînes YouTube**
- [**TechLinked**](https://www.youtube.com/@TechLinked) : Résumés rapides et dynamiques de l'actualité technologique.  
- [**Linus Tech Tips**](https://www.youtube.com/@LinusTechTips) : Tests, critiques et innovations technologiques.  
- [**MIT Technology Review**](https://www.youtube.com/@technologyreview) : Vidéos éducatives sur les technologies émergentes.

> Astuce: Essayez d'intégrer un ou deux outils dans votre routine (ex.: écoutez des vidéos Youtube en déjeunant, écouter un podcast lors d'un déplacement, lecture avant le dodo, etc.)

## Gitlab Pages

Version longue : [ici](../annexes/gitlab_pages.md). 

1. Créer un projet Gitlab Pages/Plain Html sur Gitlab
2. S'assurer que le projet est publique
3. Aller dans Settings -­> General -­> Visibility -> Pages et changer pour "Everyone with access"
4. Cloner le repo sur votre ordinateur
5. Choisir un thème
6. Mettre le thème dans le dossier public de votre repo
7. Git push
8. Succès! 
9. Allez dans Deploy -­> Pages et vous trouverez l'URL de votre site

> Le processus est assez similaire pour Github pages.

# Précisions TP1

- Objectifs
    - Que vous touchiez aux bébelles.
    - Que vous expérimentiez avec Gitlab Pages et le format de remise.
    - Préparation pour les TP 2-3-4.
