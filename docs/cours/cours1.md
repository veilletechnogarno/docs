---
outline: deep
---
# Cours 1
## Planification
<!-- no toc -->
- [Présentation](#présentation)
- [TP1](#tp1)
- [Atelier pensée créative](#atelier-pensée-créative)

## Présentation

- Présentation personnelle
- Historique du cours
    - Musée
    - Clients réels
    - Retour au Cégep
    - Cette session
- Veille techno : un cours particulier!
    - Un cours d'exploration, il n'y a pas d'obligation de résultat
    - Un peu comme les cours projets, car vous aurez beaucoup de latitude
    - Vous devrez produire de l'information primaire en explorant et documenter cette exploration
    - On ne sait pas on s'en va ou, mais ce n'est pas la destination qui compte
- [Plan de cours](../plan-de-cours)


## TP1

- [Travail pratique 1](../travaux/tp1)

## **Atelier pensée créative (divergence et convergence)**

### **Objectif**  
Stimuler la créativité en générant des idées, en explorant les outils à disposition, et en combinant ces éléments pour concevoir des concepts innovants.  

### **1. Génération d’idées : "Ça s'rait le fun que…" (Divergence)**  
**Durée** : 15 minutes en individuel (avec un thème distribué aléatoirement)
**Objectif** : Brainstormer des idées de technologies ou gadgets sans se soucier de la faisabilité.  
**Instructions** :  
- Individuellement, vous devez compléter la phrase **"Ça s'rait le fun que…"**.  
- Tentez vos idées les plus originales/farfelues.  
- Notez toutes les idées sur un tableau/un papier/dans un fichier txt.  
- Mettez l'accent sur la **quantité** et **l’ouverture d’esprit**.  

**Exemples** :  
- "Ça s'rait le fun que… mes chaussures comptent mes pas et me motivent avec des encouragements."  
- "Ça s'rait le fun que… le banc du parc affiche mon rythme cardiaque quand je m’assois dessus."  

---
### **2. Présentation des outils à disposition (Convergence)**   
---

### **3. Mélange et co-création (Divergence et Convergence)**  
**Durée** : 30 minutes (en équipe par thème)
**Objectif** : Combiner les idées générées avec les outils disponibles pour créer des concepts concrets.  
**Instructions** :  
- Rejoindre les étudiants ayant le même thème que vous.
- Chaque groupe choisit 2 à 3 idées du brainstorming et réfléchit à comment les réaliser en utilisant les outils présentés.  
- Retenez la meilleure idée à présenter (voter au besoin, mais idéalement atteindre un concensus d'équipe).

**Livrable attendu** :
- Une courte présentation par groupe, incluant :  
  - Le concept final (nom, description, utilité).  
  - Un croquis ou une représentation visuelle.  
  - Les outils nécessaires pour le réaliser.  
  
---

## Thèmes de l'atelier : Pensée Créative

1. **Internet des objets (IoT) pour la maison du futur**  
Créer des objets connectés et intelligents qui améliorent la vie quotidienne à la maison. Par exemple, des appareils capables d’interagir entre eux, d’optimiser la consommation d’énergie ou d’offrir de nouveaux services aux utilisateurs.

2. **Technologies pour le transport du futur**  
Explorer les innovations technologiques pour le transport, telles que les véhicules autonomes, les systèmes de mobilité partagée, ou des solutions pour améliorer l'efficacité et la durabilité des transports.

3. **Technologies pour la gestion de l’énergie**  
Imaginer des solutions pour optimiser la consommation énergétique à l’échelle individuelle, industrielle ou urbaine, avec l’aide de capteurs, de systèmes intelligents ou d’appareils écoénergétiques.

4. **Technologies de la ville intelligente**  
Développer des technologies pour rendre les villes plus connectées, durables et efficaces, par exemple en optimisant les infrastructures publiques, la gestion des déchets, l’éclairage ou la circulation.

5. **Systèmes de notification et alertes intelligentes**  
Concevoir des systèmes intelligents capables d'envoyer des alertes personnalisées en fonction des besoins spécifiques des utilisateurs, pour des situations d'urgence, des événements importants ou des notifications liées à la sécurité et la gestion des risques.
