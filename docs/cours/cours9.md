---
outline: deep
---
# Cours 9
# Planification
<!-- no toc -->
- [PDPPE](#programme-de-développement-professionnel-du-personnel-enseignant)
- [TP3](#tp3)
- [Déroulement des cours](#déroulement-des-cours-dici-à-la-fin-de-la-session)

# Programme de développement professionnel du personnel enseignant

- De kossé?
- Vidéo

# Tp3

https://gitlab.com/veilletechnogarno/contenu/-/blob/main/Tp3A24.pdf?ref_type=heads

# Déroulement des cours d'ici à la fin de la session

- Semaine 9 à 14
  - 1 scrum par équipe (max 20-25 min)
  - Expérimentations 
    - Matérielles 9-12
    - Logicielles 12-14
- Semaine 15
  - Présentation des prototypes

