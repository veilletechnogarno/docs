---
outline: deep
---
# Cours 5
# Planification
<!-- no toc -->
- [TP2](#tp2)
- [Exemples](#5-exemples-pour-relancer-le-processus-créatif)
- [Discussion](#discussions-et-formation-des-équipes)
- [Travail en équipe](#travail-sur-les-propositions)

# Tp2

https://gitlab.com/veilletechnogarno/contenu/-/blob/main/Tp2A24.pdf?ref_type=heads

# 5 exemples pour relancer le processus créatif

https://youtu.be/TaMiV3Wus9c?feature=shared&t=134

https://www.youtube.com/watch?v=dT-jljCXzRE

https://www.youtube.com/watch?v=SRYtn0j5ccA

https://www.youtube.com/watch?v=2lXh2n0aPyw

https://www.youtube.com/watch?v=CWwee62DW3U

# Discussions et formation des équipes

Objectifs 
1. Former des équipes de 3 à 5
2. Définir deux propositions sommaires
3. Tenir compte du nombre de coéquipiers pour définir l'ampleur du projet. Au besoin, changer les équipes ou changer le projet.

# Travail sur les propositions

Suivre les instructions du document SuiviÉquipe : https://gitlab.com/veilletechnogarno/contenu/-/blob/main/SuiviEquipe.docx?ref_type=heads

